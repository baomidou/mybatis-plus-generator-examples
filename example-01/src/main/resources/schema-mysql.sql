
DROP TABLE IF EXISTS `sample`;
CREATE TABLE `sample`  (
                           `id` bigint(64) NOT NULL COMMENT '主键',
                           `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
                           `age` int(10) NULL DEFAULT NULL COMMENT '年龄',
                           `birthday` date NULL DEFAULT NULL COMMENT '出生日期',
                           `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
                           PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '样例' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
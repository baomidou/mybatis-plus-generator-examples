package mybatis.plus.builder;

import lombok.Getter;
import lombok.Setter;

/**
 * 测试构建者模式代码生成器
 */
@Getter
@Setter
public class CodeGenerator {
    /**
     * 数据源配置
     */
    private String datasource;
    /**
     * 包配置
     */
    private String packageConfig;
    /**
     * 策略配置
     */
    private String strategyConfig;

    /**
     * 执行代码生成
     */
    public void execute() {
        System.out.println(datasource + "，" + packageConfig + "，" + strategyConfig);
    }
}

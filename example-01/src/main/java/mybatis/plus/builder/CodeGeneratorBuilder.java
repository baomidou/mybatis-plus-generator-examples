package mybatis.plus.builder;

/**
 * 代码生成器构建模式
 */
public class CodeGeneratorBuilder {
    private CodeGenerator codeGenerator;

    public CodeGeneratorBuilder(CodeGenerator codeGenerator) {
        this.codeGenerator = codeGenerator;
    }

    public static CodeGeneratorBuilder create(String datasource) {
        CodeGenerator cg = new CodeGenerator();
        cg.setDatasource(datasource);
        return new CodeGeneratorBuilder(cg);
    }

    public CodeGeneratorBuilder packageConfig(String packageConfig) {
        codeGenerator.setPackageConfig(packageConfig);
        return this;
    }

    public CodeGeneratorBuilder strategyConfig(String strategyConfig) {
        codeGenerator.setStrategyConfig(strategyConfig);
        return this;
    }

    public void execute() {
        codeGenerator.execute();
    }
}

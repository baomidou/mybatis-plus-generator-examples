package mybatis.plus.builder;

public class Test {

    public static void main(String[] args) {
        // 普通模式
        CodeGenerator codeGenerator = new CodeGenerator();
        codeGenerator.setDatasource("mysql");
        codeGenerator.setPackageConfig("com.baomidou");
        codeGenerator.setStrategyConfig("驼峰命名");
        codeGenerator.execute();

        // 构建者模式
        CodeGeneratorBuilder.create("mysql").packageConfig("com.baomidou")
                .strategyConfig("驼峰命名").execute();
    }
}

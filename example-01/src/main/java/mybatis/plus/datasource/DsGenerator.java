package mybatis.plus.datasource;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.IColumnType;
import mybatis.plus.generator.CodeGenerator;

public class DsGenerator {

    public static void main(String[] args) {
        FastAutoGenerator.create(new DataSourceConfig.Builder(CodeGenerator.URL,
                "root", "").typeConvert(new MySqlTypeConvert() {
            @Override
            public IColumnType processTypeConvert(GlobalConfig config, String fieldType) {
                if ("double".equals(fieldType)) {
                    return DbColumnType.DOUBLE;
                }
                return super.processTypeConvert(config, fieldType);
            }
        }));
    }
}

package mybatis.plus.injection;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.TemplateType;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.engine.VelocityTemplateEngine;
import mybatis.plus.generator.CodeGenerator;

import java.io.File;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * 代码生成
 */
public class InjectionGenerator {

    /**
     * 执行 run
     */
    public static void main(String[] args) throws SQLException {
        String pkgPath = System.getProperty("user.dir") + "/src/main/java";
        FastAutoGenerator.create(CodeGenerator.URL, "root", "")
                // 全局配置
                .globalConfig(builder -> builder.outputDir(pkgPath).author("青苗")
                        .fileOverride().disableOpenDir())
                // 包配置
                .packageConfig(builder -> builder.parent("com.baomidou"))
                // 模板配置
                .templateConfig(builder -> builder.disable(TemplateType.XML, TemplateType.CONTROLLER))
                // 注入配置
                .injectionConfig(builder -> builder.beforeOutputFile((tableInfo, objectMap) -> {
                            // 这里是预处理输出模板信息
                            System.out.println("tableInfo: " + tableInfo.getEntityName() + " objectMap: " + objectMap.size());
                        })
                        // 这里是自定义传入模板参数值
                        .customMap(Collections.singletonMap("test", "baomidou"))
                        .customFile(new HashMap<String, String>() {{
                            // 这里可以自定义自己的模板
                            put("hi.txt", "/templates/hi.vm");
                            put("sample.vue", "/templates/vue.vm");
                        }})
                        .build())
                // 策略配置 sample
                .strategyConfig((scanner, builder) -> builder.addInclude(scanner.apply("请输入表名？")))
                // 模板引擎的某个方法
                .templateEngine(new VelocityTemplateEngine() {
                    @Override
                    protected void outputCustomFile(Map<String, String> customFile, TableInfo tableInfo, Map<String, Object> objectMap) {
                        String otherPath = getPathInfo(OutputFile.other);
                        customFile.forEach((key, value) -> {
                            String fileName = String.format((otherPath + File.separator + "%s"), key);
                            outputFile(new File(fileName), objectMap, value);
                        });                    }
                })
                // 执行
                .execute();
    }
}

package mybatis.plus.generator;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;

import java.sql.SQLException;

/**
 * 代码生成
 */
public class CodeGenerator {
    public static final String URL = "jdbc:mysql://localhost:3306/test?useSSL=false&useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC";

    /**
     * 执行 run
     */
    public static void main(String[] args) throws SQLException {
        FastAutoGenerator.create(URL, "root", "")
                // 全局配置
                .globalConfig(builder -> builder.outputDir("D:\\码问\\code"))
                // 策略配置
                .strategyConfig(builder -> builder.addInclude("sample"))
                // 执行
                .execute();
    }
}

package mybatis.plus.template;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.TemplateType;
import mybatis.plus.generator.CodeGenerator;

import java.sql.SQLException;

/**
 * 代码生成
 */
public class TemplateGenerator {

    /**
     * 执行 run
     */
    public static void main(String[] args) throws SQLException {
        String pkgPath = System.getProperty("user.dir") + "/src/main/java";
        FastAutoGenerator.create(CodeGenerator.URL, "root", "")
                // 全局配置
                .globalConfig(builder -> builder.outputDir(pkgPath).author("青苗")
                        .fileOverride().disableOpenDir())
                // 包配置
                .packageConfig(builder -> builder.parent("com.baomidou"))
                // 模板配置
                .templateConfig(builder -> builder.disable(TemplateType.XML, TemplateType.CONTROLLER))
                // 策略配置 sample
                .strategyConfig((scanner, builder) -> builder.addInclude(scanner.apply("请输入表名？")))
                // 执行
                .execute();
    }
}

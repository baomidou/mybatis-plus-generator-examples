package mybatis.plus.pkg;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import mybatis.plus.generator.CodeGenerator;

import java.sql.SQLException;
import java.util.HashMap;

/**
 * 代码生成
 */
public class PkgGenerator {

    /**
     * 执行 run
     */
    public static void main(String[] args) throws SQLException {
        String pkgPath = System.getProperty("user.dir") + "/src/main/java";
        String pkgXml = System.getProperty("user.dir") + "/src/main/resource";
        FastAutoGenerator.create(CodeGenerator.URL, "root", "")
                // 全局配置
                .globalConfig(builder -> builder.outputDir(pkgPath).author("青苗")
                        .fileOverride().disableOpenDir())
                // 包配置
//                .packageConfig(builder -> builder.parent("com.baomidou").entity("po")
//                        .pathInfo(new HashMap<OutputFile, String>() {{
//                            put(OutputFile.mapperXml, pkgXml);
//                            put(OutputFile.service, pkgPath);
//                        }}))
                .packageConfig((scanner, builder) -> builder.parent("com.baomidou." + scanner.apply("请输入您的模块名称？")))
                // 策略配置 sample
                .strategyConfig((scanner, builder) -> builder.addInclude(scanner.apply("请输入表名？")))
                // 执行
                .execute();
    }
}
